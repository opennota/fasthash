fasthash [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](http://godoc.org/gitlab.com/opennota/fasthash?status.svg)](http://godoc.org/gitlab.com/opennota/fasthash) [![Pipeline status](https://gitlab.com/opennota/fasthash/badges/master/pipeline.svg)](https://gitlab.com/opennota/fasthash/commits/master)
========

The fast-hash is a simple, robust, and efficient general-purpose hash function.


## Install

    go get -u gitlab.com/opennota/fasthash
